import { addFaviconOverlay } from "../index";

const overlayImage = document.getElementById("overlay") as HTMLImageElement;
const overlay = addFaviconOverlay(overlayImage);

document.addEventListener("mousemove", (event: MouseEvent) => {
  const { clientX, clientY } = event;
  overlay.position = {
    centered: true,
    x: (32 * clientX) / window.innerWidth,
    y: (32 * clientY) / window.innerHeight,
  };
});
