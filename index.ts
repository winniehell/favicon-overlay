export interface FaviconOverlayPosition {
  x: number;
  y: number;
  centered?: boolean;
}

const makeReactive = <T>(object: T): T => {
  const reactiveObject = {};
  Object.keys(object).forEach((key) => {
    Object.defineProperty(reactiveObject, key, {
      get() {
        return object[key];
      },
      set(value) {
        if (value == object[key]) {
          return;
        }
        requestAnimationFrame(FaviconOverlay.render);
        return (object[key] = value);
      },
    });
  });
  return reactiveObject as T;
};

export class FaviconOverlay {
  static canvas: HTMLCanvasElement = null;
  static faviconElement: HTMLLinkElement = null;
  static faviconImage: HTMLImageElement = null;
  static overlays: FaviconOverlay[] = [];

  private _position: FaviconOverlayPosition = makeReactive<FaviconOverlayPosition>(
    {
      x: 0,
      y: 0,
      centered: false,
    }
  );

  get position(): FaviconOverlayPosition {
    return this._position;
  }

  set position(value: FaviconOverlayPosition) {
    Object.assign(this._position, value);
  }

  constructor(public image: HTMLImageElement) {
    FaviconOverlay.overlays.push(this);
    FaviconOverlay.initialize().then(() =>
      requestAnimationFrame(FaviconOverlay.render)
    );
  }

  static get isInitialized() {
    return Boolean(this.canvas);
  }

  static get faviconUrl() {
    const url = this.faviconElement.dataset.originalHref;
    if (url) {
      return url;
    }
    return (this.faviconElement.dataset.originalHref = this.faviconElement.href);
  }

  static initialize({
    faviconSelector = 'link[rel="icon"]',
  } = {}): Promise<void> {
    if (FaviconOverlay.isInitialized) {
      return Promise.resolve();
    }

    FaviconOverlay.canvas = document.createElement("canvas");
    FaviconOverlay.faviconElement = document.querySelector(faviconSelector);

    const image = document.createElement("img");
    image.src = FaviconOverlay.faviconUrl;
    return new Promise<void>((resolve) =>
      image.addEventListener("load", () => {
        FaviconOverlay.canvas.width = image.width;
        FaviconOverlay.canvas.height = image.height;
        FaviconOverlay.faviconImage = image;
        resolve();
      })
    );
  }

  static render() {
    const context = FaviconOverlay.canvas.getContext("2d");
    context.clearRect(
      0,
      0,
      FaviconOverlay.faviconImage.width,
      FaviconOverlay.faviconImage.height
    );
    context.drawImage(
      FaviconOverlay.faviconImage,
      0,
      0,
      FaviconOverlay.faviconImage.width,
      FaviconOverlay.faviconImage.height,
      0,
      0,
      FaviconOverlay.faviconImage.width,
      FaviconOverlay.faviconImage.height
    );

    FaviconOverlay.overlays.forEach((overlay) => {
      let { x, y } = overlay.position;

      if (overlay.position.centered) {
        x -= overlay.image.width / 2;
        y -= overlay.image.height / 2;
      }

      context.drawImage(
        overlay.image,
        0,
        0,
        overlay.image.width,
        overlay.image.height,
        x,
        y,
        overlay.image.width,
        overlay.image.height
      );
    });

    FaviconOverlay.faviconElement.href = FaviconOverlay.canvas.toDataURL();
  }
}

export const addFaviconOverlay = (image: HTMLImageElement) =>
  new FaviconOverlay(image);
